/*class CreatNewUser {
    constructor (name, surname, birthday) {
        this.firstName = name;
        this.lastName = surname;
        this.birthday = birthday;
    }
    getLogin() {
        return (this.firstName[0] + this.lastName).toLocaleLowerCase();
    }

    getAge() {
        return new Date().getFullYear() - this.birthday.substr(-4);
    }

    getPassword() {
        return this.firstName[0].toLocaleUpperCase() + this.lastName.toLocaleLowerCase() + this.birthday.substr(-4);
    }
}

const newUser = new CreatNewUser("Vincent", "Vega", "20.12.2000");*/

// #1
let arr = ["apple", 87, null, false, 25, "John", 7];

const filterBy = function(array, type) {
    let newArr = [];
    for (let i = 0; i < array.length; i++){
        if ((typeof array[i]) !== type) {
            newArr.push(array[i]);
        }
    }
    return newArr
} 

console.log(arr)
console.log(filterBy(arr, 'number'))


// ще щось таке вийшло
let arrTwo = ["cat", 15, true, null, undefined, "25", "Oscar", -27];
let type = "string";

const arrTwoNew = arrTwo.filter((item) => typeof item !== type);
console.log(arrTwo)
console.log(arrTwoNew)


// #2
const words = [
    // "шибениця",
    // "програма",
    // "подорож",
    // "тістечко",
    // "банан",
    // "валізка",
    // "огризок"
    "рак",
    'ліс',
    'кіт',
    'бот'
    ];

    // Обираємо випадкове слово
    let word = words[Math.floor(Math.random() * words.length)];

    // Створюємо кінцевий масив
    let answerArray = [];
            for (let i = 0; i < word.length; i++) {
            answerArray[i] = "_";
    }

    let remainingLetters = word.length;
    // Ігровий цикл
    while (remainingLetters > 0) {
    // Демонструємо стан гри
        alert(answerArray.join(" "));
    
    // Запитуємо варіант відповіді
        let guess = prompt(`Вгадайте букву, або натисніть Скасувати для
        виходу з гри.`);
        
        if (guess === null) {
    // Вихід з ігрового циклу
        break;
        } else if (guess.length !== 1) {
            alert("Будь ласка, введіть лише одну літеру.");
            } else {
    // Оновлення стану гри
                for (let j = 0; j < word.length; j++) {
                    if (word[j] === guess) {
                    answerArray[j] = guess;
                    remainingLetters--;
                }  
            }
        }
    // Кінець ігрового циклу
    }
    // Відображаємо відповідь та вітаємо ігрока
    alert(answerArray.join(" ")); 
    alert(`Чудово! Було загадано слово "${word}" `);