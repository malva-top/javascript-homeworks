class Driver {
    constructor(fullName, age){
        this.fullName = fullName;
        this.age = age;
    }
}

class Engine {
    constructor(power, company){
        this.power = power;
        this.company = company;
    }
}  
 
class Car extends Driver {
    constructor(marka, carClass, weight, fullName, age) {
      super (marka, carClass, weight, fullName, age);
      this.marka = marka;
      this.carClass = carClass;
      this.weight = weight;
    }
}

// class Car extends Engine{
//     constructor(marka, carClass, weight, power, company) {
//       super (marka, carClass, weight, power, company);
//       this.marka = marka;
//       this.carClass = carClass;
//       this.weight = weight;
//     }
// }

Car.prototype.start = function(){
    document.write("Go!" + "</br>");
    console.log("Go!")
};

Car.prototype.stop = function(){
    document.write("Stop!" + "</br>");
    console.log("Stop!")
};
  
Car.prototype.turnRight = function(){
    document.write("Turn Right!" + "</br>");
    console.log("Turn Right!")
};
  
Car.prototype.turnLeft = function(){
    document.write("Turn Left!" + "</br>");
    console.log("Turn Left!")
};

Car.prototype.toString = function () {
    console.log(a, b, c);
} 



const a = new Car("VW", "B", 3000, Driver, Engine);
const b = new Driver ("John Doe", 10)
const c = new Engine (300, "GM")

a.start();
a.stop();
a.turnRight();
a.turnLeft();
a.toString();

class Lorry extends Car {
    constructor(marka, carClass, weight, driver, engine, carrying){
        super (marka, carClass, weight, driver, engine);
        this.carrying = carrying;
    }
    
}

class SportCar extends Car {
    constructor(marka, carClass, weight, driver, engine, speed){
        super (marka, carClass, weight, driver, engine); 
        this.speed = speed;
    }
}