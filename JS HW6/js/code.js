// #1
class Worker{
    constructor (name, surname, rate, days){
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days;
    }
}

Worker.prototype.getSalary = function (){
    return this.rate * this.days;
}

const salary = new Worker ();
salary.rate = 750;
salary.days = 20;

// І так ніби теж працює, не знаю як правильніше
const salary2 = new Worker ("Olena", "Ivanova", 1000, 22);

console.log(salary.getSalary());
console.log(salary2.getSalary());
// document.write(salary.getSalary() + "</br>");
// document.write(salary2.getSalary() + "</br>");
// alert(salary.getSalary());
// alert(salary2.getSalary());

// №2
class MyString{
    constructor(str){
    }
}

MyString.prototype.reverse = function(str){
            let newStr = "";
            for (let i = str.length - 1; i >= 0; i--){
                newStr += str [i];
            }
            console.log(newStr);
        };

MyString.prototype.ucFirst = function(str){
            let newStr = str[0].toUpperCase() + str.slice(1);
            console.log(newStr);
}

MyString.prototype.ucWords = function(str){
           let arr =  str.split(" ");
           
           for (let i = 0; i < arr.length; ++i){            
                arr[i] = arr[i][0].toUpperCase() + arr[i].slice(1); 
            }
            
            str = arr.join(" ");

            console.log(str);
}

const text = new MyString ();

text.reverse("hello!");
text.ucFirst("hello, darling!");
text.ucWords("hello, my dear friend!");


// #3
class Phone{
    constructor (number, model, weight){
        this.number = number;
        this.model = model;
        this.weight = weight;
    }
}
 
Phone.prototype.receiveCall = function (name){
            console.log(`Телефонує ${name}`)
        };

Phone.prototype.getNumber = function(){
            console.log(this.number);
        };

const phone1 = new Phone (6612345678, "Nokia", 180);
const phone2 = new Phone (6712345678, "Xiaomi", 150);
const phone3 = new Phone (7312345678, "iPhone", 200);

console.log(phone1.number, phone1.model, phone1.weight);
console.log(phone2.number, phone2.model, phone2.weight);
console.log(phone3.number, phone3.model, phone3.weight);

/*не зовсім зрозуміла по завданню - вивести лише 
значення змінних чи весь об'єкт повністю, як нижче,
тож вивела і так, і так*/
console.log(phone1);
console.log(phone2);
console.log(phone3);

phone1.receiveCall("Алекс");
phone2.receiveCall("Леон");
phone3.receiveCall("Оскар");

phone1.getNumber();
phone2.getNumber();
phone3.getNumber();

// #4
class Car {
    constructor(marka, carClass, weight, driver, engine) {
      this.marka = marka;
      this.carClass = carClass;
      this.weight = weight;
      this.engine = engine;
      this.driver = driver;
    }
}

Car.prototype.start = function(){
    document.write("Go!" + "</br>");
    console.log("Go!")
};

Car.prototype.stop = function(){
    document.write("Stop!" + "</br>");
    console.log("Stop!")
};
  
Car.prototype.turnRight = function(){
    document.write("Turn Right!" + "</br>");
    console.log("Turn Right!")
};
  
Car.prototype.turnLeft = function(){
    document.write("Turn Left!" + "</br>");
    console.log("Turn Left!")
};

Car.prototype.toString = function () {
    console.log(a, b, c);
} 

class Driver {
    constructor(fullName, age){
        this.fullName = fullName;
        this.age = age;
    }
}

class Engine {
    constructor(power, company){
        this.power = power;
        this.company = company;
    }
}  


const a = new Car("VW", "B", 3000, Driver, Engine);
const b = new Driver ("John Doe", 10)
const c = new Engine (300, "GM")

a.start();
a.stop();
a.turnRight();
a.turnLeft();
a.toString();

class Lorry extends Car {
    constructor(marka, carClass, weight, driver, engine, carrying){
        super (marka, carClass, weight, driver, engine);
        this.carrying = carrying;
    }
    
}

class SportCar extends Car {
    constructor(marka, carClass, weight, driver, engine, speed){
        super (marka, carClass, weight, driver, engine); 
        this.speed = speed;
    }
}

// #5
class Animal {
    constructor(food, location) {
      this.food = food;
      this.location = location;
     }
}
Animal.prototype.makeNoise = function () {
    console.log(`This animal is silent!`);
};
    
Animal.prototype.eat = function () {
    console.log(`This animal is eating!`);
};
    
Animal.prototype.sleep = function () {
    console.log(`This animal is sleeping!`);
};
    
const anim = new Animal ();
    
anim.makeNoise();
anim.eat();
anim.sleep();
    
class Dog extends Animal {
    constructor(food, location, name) {
        super(food, location);
        this.name = name;
    }
}

Dog.prototype.makeNoise = function () {  
    console.log('This animal is barking');
}

Dog.prototype.getVoice = function(){
    console.log("bow-wow");
}

Dog.prototype.eat = function () {  
    this.getVoice();
}

const dog = new Dog ("bones", "yard", "Barsik")
console.log(dog);
dog.makeNoise();
dog.eat();  

class Cat extends Animal {
    constructor(food, location, colorFur) {
        super(food, location, );
        this.colorFur = colorFur;
    }
}

Cat.prototype.getVoice = function(){
    console.log("mew-mew");
}

Cat.prototype.scratch = function(){
    console.log("The cat is scratching");
}

Cat.prototype.makeNoise = function(){
    this.getVoice();
}

Cat.prototype.eat = function(){
    this.scratch();
}

const cat = new Cat("fish", "loft", "red");
console.log(cat);
console.log(cat.colorFur);
console.log(cat.food);
cat.makeNoise();
cat.eat();


class Horse extends Animal {
    constructor(food, location, speed) {
        super(food, location);
        this.speed = speed;
    }
}

Horse.prototype.getVoice = function(){
    console.log("neigh-neigh");
}

Horse.prototype.run = function(){
    console.log("The horse is running");
}

Horse.prototype.makeNoise = function(){
    this.getVoice();
}

Horse.prototype.eat = function(){
    this.run();
}

const horse = new Horse("hay", "stable", 25);
console.log(horse);
console.log(horse.location);
horse.makeNoise();
horse.eat();

class Veterenarion {
    constructor(food, location) {
      this.food = food;
      this.location = location;
     }
}

Veterenarion.prototype.treatAnimal = function(){
    console.log(`
    food: ${this.food}
    location: ${this.location}`);
}


Veterenarion.prototype.main = function(){
    
    let arr = [cat, dog, horse];
    console.log(arr)

    for (let i = 0; i < arr.length; i++){
        console.log(`The ${arr[i]} came to the vet`)
    }
}

const patient = new Veterenarion("bread", "zoo");
patient.treatAnimal();
patient.main(cat);
