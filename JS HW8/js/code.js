// Header
const header = document.createElement('header');
document.body.prepend(header);
header.classList.add("header");
header.textContent = "Header";

const navInHeader = document.createElement('nav');
header.append(navInHeader);
navInHeader.classList.add('nav-header');
navInHeader.textContent = "nav";

navInHeader.setAttribute('contenteditable', 'true');
navInHeader.onclick = () => {
    navInHeader.textContent = "";
}

const divForSections = document.createElement('div');
header.after(divForSections);
divForSections.classList.add('div-section');

// Section Left
const sectionLeft = document.createElement('section');
divForSections.prepend(sectionLeft);
sectionLeft.classList.add('section-left');
sectionLeft.textContent = 'section';

const headerSectionLeft = document.createElement('header');
sectionLeft.append(headerSectionLeft)
headerSectionLeft.classList.add('header-section');
headerSectionLeft.textContent = "Header";

const input = document.createElement('input');
input.setAttribute("value", " ");

headerSectionLeft.addEventListener("click", () =>{
    headerSectionLeft.textContent = " "; 
    headerSectionLeft.append(input);
    input.getAttribute("value");
})

// Article First
const articleFirst = document.createElement('article');
sectionLeft.append(articleFirst);
articleFirst.classList.add('article')
articleFirst.textContent = 'article';

const headerArticleFirst = document.createElement('header');
articleFirst.append(headerArticleFirst);
headerArticleFirst.classList.add('header-article');
headerArticleFirst.textContent = 'header';

const paragraphFirstInArticleFirst = document.createElement('p');
articleFirst.append(paragraphFirstInArticleFirst);
paragraphFirstInArticleFirst.classList.add('paragraph');
paragraphFirstInArticleFirst.textContent = "p";

const divInArticleFirst = document.createElement('div');
articleFirst.append(divInArticleFirst);
divInArticleFirst.classList.add('div-article');

const paragraphSecondInArticleFirst = document.createElement('p'); 
divInArticleFirst.prepend(paragraphSecondInArticleFirst);
paragraphSecondInArticleFirst.classList.add('paragraph-art-first');
paragraphSecondInArticleFirst.textContent = 'p';

const aside = document.createElement('aside'); 
divInArticleFirst.append(aside);
aside.classList.add('aside');
aside.textContent = 'aside';

const footerInArticleFirst = document.createElement('footer');
articleFirst.append(footerInArticleFirst);
footerInArticleFirst.classList.add('footer-art-first');
footerInArticleFirst.textContent = 'footer';

// Article Second
const articleSecond = document.createElement('article');
sectionLeft.append(articleSecond);
articleSecond.classList.add('article');
articleSecond.textContent = 'article';

const headerArticleSecond = document.createElement('header');
articleSecond.append(headerArticleSecond);
headerArticleSecond.classList.add('header-article');
headerArticleSecond.textContent = "header";

const paragraphFirstInArticleSecond = document.createElement('p'); 
articleSecond.append(paragraphFirstInArticleSecond);
paragraphFirstInArticleSecond.classList.add('paragraph');
paragraphFirstInArticleSecond.textContent = 'p';

const paragraphSecondInArticleSecond = document.createElement('p'); 
articleSecond.append(paragraphSecondInArticleSecond);
paragraphSecondInArticleSecond.classList.add('paragraph');
paragraphSecondInArticleSecond.textContent = "p";

const footerInArticleSecond = document.createElement('footer');
articleSecond.append(footerInArticleSecond);
footerInArticleSecond.classList.add('footer-art-first');
footerInArticleSecond.textContent = "footer";

const footerInSectionLeft = document.createElement('footer');
sectionLeft.append(footerInSectionLeft);
// sectionLeft.insertAdjacentHTML('beforeend', '<footer>footer</footer>');
footerInSectionLeft.classList.add('footer-section');
footerInSectionLeft.textContent = 'footer';

//Section Right
const sectionRight = document.createElement('section');
divForSections.append(sectionRight);
sectionRight.classList.add('section-right');
sectionRight.innerText = 'section';

const headerSectionRight = document.createElement('header');
// sectionRight.insertAdjacentHTML('afterend', '<header>header</header>');
sectionRight.append(headerSectionRight);
headerSectionRight.classList.add('header-section');
headerSectionRight.innerText = 'header';

const navInSectionRight = document.createElement('nav');
sectionRight.append(navInSectionRight);
navInSectionRight.classList.add('nav');
navInSectionRight.textContent = 'nav';

// const input = document.createElement('input');
// navInSectionRight.onclick = () => {
//     navInSectionRight.insertAdjacentHTML('beforeend', '<input></input>')
// }


// Footer
const footer = document.createElement('footer');
divForSections.after(footer);
footer.classList.add('footer');
footer.textContent = "Footer";



// console.dir(header);
// console.info();




