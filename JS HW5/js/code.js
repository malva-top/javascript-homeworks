/*Створити об'єкт "Документ", де визначити властивості
"Заголовок, тіло, футер, дата".
Створити в об'єктi вкладений об'єкт - "Додаток".
Створити в об'єкті "Додаток" вкладені об'єкти,
"Заголовок, тіло, футер, дата".
Створити методи для заповнення та відображення документа.*/

const doc = {
    title: "Заголовок",
    body: "Тіло",
    footer: "Футер",
    date: "Дата",
    application: {
        title: {
            t1: "Заголовок 1",
            t2: "Заголовок 2"
        },
        body: {
            b1: "Тіло 1",
            b2: "Тіло 2"
        },
        footer: {
            f1: "Футер 1",
            f2: "Футер 2"
        },
        date: {
            d1: "Дата 1",
            d2: "Дата 2"
        }
    },
    
    show: function () {
        document.write("<p>Title: " + this.title);
        document.write("<p>Body: " + this.body);
        document.write(`<p>Footer: ${doc.footer}`);
        document.write(`<p>Date: ${doc.date}`);
        document.write(`<p>Application 1/1: ${doc.application.title.t1}`);
        document.write(`<p>Application 2/1: ${doc.application.body.b1}`);
        document.write("<p>Application 3/1: " + doc.application.footer.f1);
        document.write("<p>Application 4/1: " + doc.application.date.d1);
        document.write(`<p style='color: teal;'>Application 1/2: ${doc.application.title.t2}`);
        document.write(`<p style='color: teal;'>Application 2/2: ${doc.application.body.b2}`);
        document.write("<p style='color: teal;'>Application 3/2: " + doc.application.footer.f2);
        document.write("<p style='color: teal;'>Application 4/2: " + doc.application.date.d2 + "<hr color='green'");
    },

    changeTitle: function(titleAny) {
        this.title = titleAny;
    },

    changeApp: function(b2New) {
        this.application.body.b2 = b2New;
    }
}

doc.show();

doc.changeTitle("<span style='font-size: 20px; color: blue'> Інший Заголовок </span>");
doc.changeApp("<span style='color: red;'> Зовсім інше body)");

doc.show();

// Просто спробувала вивести інакше
// document.getElementById("body").innerHTML = `<span style='color:orange; font-weight: 900;'>${doc.body}</span>`;
// document.getElementById("footer").innerHTML = `<span style='background:orange; font-weight: 900;'>${doc.application.footer.f1}</span>`;
