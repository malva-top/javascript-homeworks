// Завдання 1
let array = [2,5,7,4];
function map(exp, array){
    let arrayNew = [];
    for (let i = 0; i < array.length; i++){
     arrayNew[i] = exp(array[i]);
}
return arrayNew;
}

const exp = (a) => {
    return a ** 2;
}

document.write("<p> Вхідний масив: " + array + "</br>");
document.write("<p> Новий масив: " + map(exp,[2,5,7,4]));

// Завдання 2
const age = prompt ("Введи свій вік");

function checkAge() {
    /*if (age > 18){
        return true;
    }else{
        return confirm ("Батьки дозволили?!");
    }*/

    return (age > 18) ? true :  confirm ("Батьки дозволили?!");
    
    // або так  
    // return (age > 18) || confirm ("Батьки дозволили?!");

}
checkAge();