// № 1
var arrOne = ['a', 'b', 'c'];
var arrTwo = [1, 2, 3];

document.write("<p>1. Дані два масиви: ['a', 'b', 'c'] та [1, 2, 3]. Об'єднайте їх разом.");
document.write("<p>Результат: " + arrOne.concat(arrTwo) + "<hr color='gray'>");


// № 2
let arrRes2 = arrOne.push(1,2,3);

document.write("<p>2. Дан масив ['a', 'b', 'c']. Додайте йому до кінця елементи 1, 2, 3.");
document.write("<p>Результат: " + arrOne + "<hr color='gray'>");

//  № 3

document.write("<p>3. Дан масив [1, 2, 3]. Зробіть із нього масив [3, 2, 1].");
document.write("<p>Результат: " + arrTwo.reverse() + "<hr color='gray'>");

//  № 4
arrTwo = [1, 2, 3];
let arrRes4 = arrTwo.push(4,5,6);

document.write("<p>4. Дан масив [1, 2, 3]. Додайте йому до кінця елементи 4, 5, 6.");
document.write("<p>Результат: " + arrTwo + "<hr color='gray'>");

//  № 5
arrTwo = [1, 2, 3];
let arrRes5 = arrTwo.unshift(4,5,6);

document.write("<p>5. Дан масив [1, 2, 3]. Додайте йому на початок елементи 4, 5, 6.");
document.write("<p>Результат: " + arrTwo + "<hr color='gray'>");

//  № 6
let arrThree = ['js', 'css', 'jq'];

document.write("<p>6. Дан масив ['js', 'css', 'jq']. Виведіть перший елемент на екран.");
document.write("<p>Результат: " + arrThree[0] + "<hr color='gray'>");

//  № 7
var arrFour = [1, 2, 3, 4, 5];

document.write("<p>7. Дан масив [1, 2, 3, 4, 5]. За допомогою методу slice запишіть нові елементи [1, 2, 3].");
document.write("<p>Результат: " + arrFour.slice(0, 3) + "<hr color='gray'>");

//  № 8
arrFour = [1, 2, 3, 4, 5];
let arrRes8 = arrFour.splice(1, 2);

document.write("<p>8. Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 4, 5].");
document.write("<p>Результат: " + arrFour + "<hr color='gray'>");

//  № 9
arrFour = [1, 2, 3, 4, 5];
let arrRes9 = arrFour.splice(2, 0, 10);

document.write("<p>9. Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 2, 10, 3, 4, 5].");
document.write("<p>Результат: " + arrFour + "<hr color='gray'>");

//  № 10
arrFive = [3, 4, 1, 2, 7];

document.write("<p>10. Дан масив [3, 4, 1, 2, 7]. Відсортуйте його.");
document.write("<p>Результат: " + arrFive.sort() + "<hr color='gray'>");

//  № 11
var arrSix = ['Привіт, ', 'світ', '!'];

let arrRes11 = arrSix.splice(1, 1, "мир");

arrSix = arrSix.join("");

document.write("<p>11. Дан масив з елементами 'Привіт, ', 'світ' і '!'. Потрібно вивести на екран фразу 'Привіт, мир!'.");
document.write("<p>Результат: " + arrSix + "<hr color='gray'>");

//  № 12
arrSix = ['Привіт, ', ' світ', '!'];

let arrRes12 = arrSix.splice(0, 1, "Поки,");

arrSix = arrSix.join("");

document.write("<p>12. Дан масив ['Привіт, ', 'світ', '!']. Необхідно записати в нульовий елемент цього масиву слово 'Поки, ' (тобто замість слова 'Привіт, ' буде 'Поки, ')");
document.write("<p>Результат: " + arrSix + "<hr color='gray'>");

//  № 13
let arr1 = [1, 2, 3, 4, 5];

let arr2 = new Array(1, 2, 3, 4, 5);

document.write("<p>13. Створіть масив arr з елементами 1, 2, 3, 4, 5 двома різними способами.");
document.write("<p>Результат: " + arr1);
document.write("<p>Результат: " + arr2 + "<hr color='gray'>");

// № 14
let arr = [
    ['блакитний', 'червоний', 'зелений'],
    ['blue', 'red', 'green'],
];

document.write("<p>14. Дан багатовимірний масив arr: var arr = {'ru':['блакитний', 'червоний', 'зелений'],'en':['blue', 'red', 'green'],};" + "</br>" + "Виведіть за його допомогою слово 'блакитний' 'blue' .");
document.write("<p>Результат: " + "'" + arr[0][0] + "' " + "'" + arr[1][0] + "'" + "<hr color='gray'>");

//  № 15
let arr3 =  ['a', 'b', 'c', 'd'];

let str = ("'" + arr3[0] + "+" + arr3[1] + ", " + arr3[2] + "+" + arr3[3] + "'");

document.write("<p>15. Створіть масив arr = ['a', 'b', 'c', 'd'] і за допомогою його виведіть на екран рядок 'a+b, c+d'.");
document.write("<p>Результат: " + str + "<hr color='gray'>");

//  № 16
let qtyElements = prompt("На скільки елементів ти хочеш масив?", "введи число");

let arrUser = new Array(qtyElements);

arrUser.length = qtyElements;

for (i = 0; i < arrUser.length; i++ ) {
    arrUser[i] = i;
    console.log(arrUser[i]);
}

document.write("<p>16. Запитайте у користувача кількість елементів масиву. Виходячи з даних, які ввів користувач, створіть масив на ту кількість елементів, яку передав користувач. У кожному індексі масиву зберігайте число, яке показуватиме номер елемента масиву." + "</br>");
document.write("<p>Масив користувача: " + arrUser + "<hr color='gray'>");

//  № 17
document.write("<p>17. Зробіть так, щоб з масиву, який ви створили вище, вивелися всі непарні числа в параграфі, а парні в спані з червоним тлом." + "</br>");

for (j = 0; j < arrUser.length; j++){
    if (j % 2 == 0){
        document.write("<span style='background-color:red;>" + arrUser[j] + "</br>");
    } else {
        document.write("<p>"  + arrUser[j] + "</p>");
    }
}

document.write("<hr color='gray'>");

//  № 18
var vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морквинка'];
 
//можна ще було використати vegetables.toString(); але в результаті не було б пробілу між елементами

document.write("<p>18. Напишіть код, який перетворює та об'єднує всі елементи масиву в одне рядкове значення. Елементи масиву будуть розділені комою."  + "</br>" + "var vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морквинка'];");
document.write("<p>Результат: " + "&quot;" + vegetables.join(", ") + "&quot;" + "<hr color='gray'>");